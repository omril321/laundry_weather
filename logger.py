__author__ = 'Omri'
import logging

LOGGER_NAME = 'laundry_weather'
LOG_FILE_NAME = LOGGER_NAME + '.log'

logger = logging.getLogger(LOGGER_NAME)
logger.setLevel(logging.DEBUG)
# create file handler which logs even debug messages
fh = logging.FileHandler(LOG_FILE_NAME)
fh.setLevel(logging.DEBUG)
# create console handler with a higher log level
ch = logging.StreamHandler()
ch.setLevel(logging.ERROR)
# create formatter and add it to the handlers
formatter = logging.Formatter('%(asctime)s - %(levelname)s : %(message)s')
fh.setFormatter(formatter)
ch.setFormatter(formatter)
# add the handlers to the logger
logger.addHandler(fh)
logger.addHandler(ch)

print "Logger file is: ", LOG_FILE_NAME


def get_logger():
    return logger

