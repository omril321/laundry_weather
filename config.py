import ConfigParser

__author__ = 'Omri'


def __generate_convert_to(type_to_cast): return lambda val: type_to_cast(val)


__convert_to_str = __generate_convert_to(str)
__convert_to_float = __generate_convert_to(float)

Config = ConfigParser.ConfigParser()
Config.read("config.ini")

# Option dictionary - type, section, option
API_KEY = ("DarkSky", "ApiKey", __convert_to_str)
LAT = ("DarkSky", "Lat", __convert_to_float)
LNG = ("DarkSky", "Lng", __convert_to_float)

MAX_PRECIP_PROB = ("LaundryConditions", "MaxPrecipProb", __convert_to_float)
MAX_CLOUD_COVER = ("LaundryConditions", "MaxCloudCover", __convert_to_float)
MIN_TEMP = ("LaundryConditions", "MinTemp", __convert_to_float)

NOTIFICATION_HOUR_STRING = ("Notification", "Hour", __convert_to_str)

MAKER_KEY = ("Maker", "MakerKey", __convert_to_str)
MAKER_EVENT_NAME = ("Maker", "MakerEventName", __convert_to_str)


def get_config(conf_dict):
    section = conf_dict[0]
    option = conf_dict[1]
    action = conf_dict[2]

    return action(Config.get(section=section, option=option))
