import forecastio
from logger import get_logger
import config
import schedule
import time
import maker_service

DAYS_THRESHOLD = 3

__author__ = 'Omri'

api_key = config.get_config(config.API_KEY)

lat = config.get_config(config.LAT)
lng = config.get_config(config.LNG)

max_precip_prob = config.get_config(config.MAX_PRECIP_PROB)
max_cloud_cover = config.get_config(config.MAX_CLOUD_COVER)
min_temp = config.get_config(config.MIN_TEMP)

notification_time = config.get_config(config.NOTIFICATION_HOUR_STRING)

# TODO: use the logger more.
logger = get_logger()


# TODO: Add filtering for good laundry days in the following week (3 days maximum)

def is_good_for_laundry(rainchance, cloudcover, max_celsius):
    return (rainchance < max_precip_prob) and (cloudcover < max_cloud_cover) and (max_celsius > min_temp)


def get_all_laundry_days():
    forecast = forecastio.load_forecast(api_key, lat, lng)
    daily = forecast.daily().data

    all_days = []
    for day in daily:
        probability_ = day.d["precipProbability"]
        cloud_ = day.d["cloudCover"]
        max_temp_ = day.d["apparentTemperatureMax"]
        is_good = is_good_for_laundry(rainchance=probability_, cloudcover=cloud_, max_celsius=max_temp_)
        all_days.append({'time': day.time, 'is_good': is_good})

    return all_days


def string_from_date(date):
    return date.strftime('%A (%d/%m)')


def get_days_to_notify_about():
    all_days = get_all_laundry_days()
    logger.debug("forecast for all days: {}".format(all_days))
    good_days_strings = [string_from_date(day['time']) for day in all_days if day['is_good']]
    num_good_days = len(good_days_strings)
    if num_good_days <= DAYS_THRESHOLD:
        logger.info("Found {} good days: {}".format(num_good_days, good_days_strings))
        return good_days_strings
    else:
        logger.info("No need to notify - number of good days is: {}, and threshold is: {}".format(num_good_days,
                                                                                               DAYS_THRESHOLD))
        return []


def excecute():
    logger.info("=========  Executing")
    notification_dates = get_days_to_notify_about()
    if len(notification_dates) > 0:
        maker_service.send_notification(notification_dates)
    else:
        logger.info("Not sending notifications.")


if __name__ == "__main__":
    logger.info("App started")
    schedule.every().day.at(notification_time).do(excecute)

    excecute()
    while True:
        schedule.run_pending()
        time.sleep(60)
