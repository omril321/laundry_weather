import config
import urllib2
import json

from logger import get_logger

logger = get_logger()

# TODO: inject the configuration

maker_event_name = config.get_config(config.MAKER_EVENT_NAME)
maker_key = config.get_config(config.MAKER_KEY)

maker_url = 'http://maker.ifttt.com'
maker_path = '/trigger/{}/with/key/{}'.format(maker_event_name, maker_key)

req = urllib2.Request(maker_url + maker_path)
req.add_header('Content-Type', 'application/json')


def __prepare_body_for_post(days_to_notify):
    return {
        'value1': ', '.join(days_to_notify)
    }


def send_notification(days_to_notify):
    body = __prepare_body_for_post(days_to_notify)
    logger.info("Sending notification with body: {}".format(body))
    response = urllib2.urlopen(req, json.dumps(body))